Requirements
=========================

We recommend having the following software packages installed, before starting with the installation of DuMu<sup>x</sup>:

  * gcc >= 7
  * cmake >= 3.13
  * git
  * pkg-config
  * paraview (to visualize the results)
  * wget (to download some config files during the installation)
  * python3 (to run the installscript)

Getting started
---------------

If these preliminaries are met, you should first decide where you would like to download all the contents to. We suggest creating a new folder and entering it. Then download and execute the installscript via

```bash
wget https://git.iws.uni-stuttgart.de/IvBu/pn5-sandbox-swe/-/raw/main/install_pn5-shallowwater.py
python3 install_pn5-shallowwater.py
```

Alternatively, you can also manually execute all steps of the script.
The installscript will download all necessary DUNE modules as well as the DuMu<sup>x</sup> module and the pn5-sandbox-swe module (swe is short for shallow water equations) and place them all in the folder `DUMUX`. While our test cases are stored in `DUMUX/pn5-sandbox-swe/appl/pn5shallowwater/dambreak` and `DUMUX/pn5-sandbox-swe/appl/pn5shallowwater/bowl`, the shallow water model itself is a part of `DUMUX/dumux` and is used in our tests.

If the installscript was executed successfully, you will also find the folder `DUMUX/pn5-sandbox-swe/build-cmake`. Here, all the executables and build-files are stored. If you would like to execute the dambreak test, you would enter the folder `DUMUX/pn5-sandbox-swe/build-cmake/appl/pn5shallowwater/dambreak` and run the commands

```bash
make test_shallowwater_dambreak
./test_shallowwater_dambreak
```

which will compile and run the executable.
After the simulation passed, `.vtu` and `.pvd` files will have been generated. These can be opened via paraview in order to visualize the results. As the `.pvd` files are an aggregation of the `.vtu` files, you could run

```bash
paraview *.pvd
```

to open all `.pvd` files and visualize them with paraview.
