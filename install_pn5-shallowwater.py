#!/usr/bin/env python3

# 
# This installs the module pn5-shallowwater and its dependencies.
# The exact revisions used are listed in the table below.
# However, note that this script may also apply further patches.
# If so, all patches are required to be the current folder, or,
# in the one that you specified as argument to this script.
# 
# 
# |      module name      |      branch name      |                 commit sha                 |         commit date         |
# |-----------------------|-----------------------|--------------------------------------------|-----------------------------|
# |  dune-localfunctions  |  origin/releases/2.9  |  3f2805875bbe5569190663c13bfcd1c49ccee7e9  |  2022-10-20 18:18:11 +0000  |
# |     dune-geometry     |  origin/releases/2.9  |  2cec5b8c5b335df922a121c3e6577e89fc3f4d13  |  2022-10-20 18:03:11 +0000  |
# |      dune-common      |  origin/releases/2.9  |  d5f5ceec68cc0dc542f175857c9bf14aa98f2c11  |  2022-10-20 18:41:15 +0000  |
# |         dumux         |  origin/releases/3.6  |  b29972cb2f2c551457a6a5113953f0e77e61e426  |  2022-10-18 07:37:37 +0000  |
# |    pn5-shallowwater   |          main         |                                            |                             |
# |       dune-grid       |  origin/releases/2.9  |  5400c3a9f25e861ae3f5e90f75d59dbb58b62f17  |  2022-11-12 20:59:15 +0000  |
# |       dune-istl       |  origin/releases/2.9  |  44ee928e99a86f8a51d517dc9d68a4702f2c0e57  |  2022-10-20 18:22:18 +0000  |

import os
import sys
import subprocess

top = "DUMUX"
os.makedirs(top, exist_ok=True)


def runFromSubFolder(cmd, subFolder):
    folder = os.path.join(top, subFolder)
    try:
        subprocess.run(cmd, cwd=folder, check=True)
    except Exception as e:
        cmdString = ' '.join(cmd)
        sys.exit(
            "Error when calling:\n{}\n-> folder: {}\n-> error: {}"
            .format(cmdString, folder, str(e))
        )


def installModule(subFolder, url, branch, revision):
    targetFolder = url.rstrip(".git").split("/")[-1]
    if not os.path.exists(targetFolder):
        runFromSubFolder(['git', 'clone', url, targetFolder], '.')
        runFromSubFolder(['git', 'checkout', branch], subFolder)
        runFromSubFolder(['git', 'reset', '--hard', revision], subFolder)
    else:
        print(
            f"Skip cloning {url} since target '{targetFolder}' already exists."
        )


def installModuleNoRevision(subFolder, url, branch):
    targetFolder = url.rstrip(".git").split("/")[-1]
    if not os.path.exists(targetFolder):
        runFromSubFolder(['git', 'clone', url, targetFolder], '.')
        runFromSubFolder(['git', 'checkout', branch], subFolder)
    else:
        print(
            f"Skip cloning {url} since target '{targetFolder}' already exists."
        )


def applyPatch(subFolder, patch):
    sfPath = os.path.join(top, subFolder)
    patchPath = os.path.join(sfPath, 'tmp.patch')
    with open(patchPath, 'w') as patchFile:
        patchFile.write(patch)
    runFromSubFolder(['git', 'apply', 'tmp.patch'], subFolder)
    os.remove(patchPath)

print("Installing dune-localfunctions")
installModule("dune-localfunctions", "https://gitlab.dune-project.org/core/dune-localfunctions.git", "origin/releases/2.9", "3f2805875bbe5569190663c13bfcd1c49ccee7e9", )

print("Installing dune-geometry")
installModule("dune-geometry", "https://gitlab.dune-project.org/core/dune-geometry.git", "origin/releases/2.9", "2cec5b8c5b335df922a121c3e6577e89fc3f4d13", )

print("Installing dune-common")
installModule("dune-common", "https://gitlab.dune-project.org/core/dune-common.git", "origin/releases/2.9", "d5f5ceec68cc0dc542f175857c9bf14aa98f2c11", )

print("Installing dumux")
installModule("dumux", "https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git", "origin/releases/3.6", "b29972cb2f2c551457a6a5113953f0e77e61e426", )

print("Installing pn5-shallowwater")
installModuleNoRevision("pn5-sandbox-swe", "https://git.iws.uni-stuttgart.de/IvBu/pn5-sandbox-swe.git", "main", )

print("Installing dune-grid")
installModule("dune-grid", "https://gitlab.dune-project.org/core/dune-grid.git", "origin/releases/2.9", "5400c3a9f25e861ae3f5e90f75d59dbb58b62f17", )

print("Installing dune-istl")
installModule("dune-istl", "https://gitlab.dune-project.org/core/dune-istl.git", "origin/releases/2.9", "44ee928e99a86f8a51d517dc9d68a4702f2c0e57", )

print("Configuring project")
runFromSubFolder(
    ['./dune-common/bin/dunecontrol', '--opts=dumux/cmake.opts', 'all'],
    '.'
)
