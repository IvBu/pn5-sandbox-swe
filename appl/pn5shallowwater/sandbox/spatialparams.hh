// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup ShallowWaterTests
 * \brief The spatial parameters for the sandbox problem.
 */
#ifndef DUMUX_SANDBOX_SPATIAL_PARAMETERS_HH
#define DUMUX_SANDBOX_SPATIAL_PARAMETERS_HH

#include <dumux/freeflow/spatialparams.hh>

namespace Dumux {

/*!
 * \ingroup ShallowWaterTests
 * \brief The spatial parameters class for the sandbox test.
 *
 */
template<class GridGeometry, class Scalar>
class SandboxSpatialParams
: public FreeFlowSpatialParams<GridGeometry, Scalar, SandboxSpatialParams<GridGeometry, Scalar>>
{
    using ThisType = SandboxSpatialParams<GridGeometry, Scalar>;
    using ParentType = FreeFlowSpatialParams<GridGeometry, Scalar, ThisType>;
    using GridView = typename GridGeometry::GridView;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    SandboxSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        bedSurface_.assign(this->gridGeometry().gridView().size(0), 0.0);  
        
        // compute the bedSurface for all elements
        const Scalar islandHeightAtCenter = getParam<Scalar>("Problem.IslandHeightAtCenter");
        const Scalar islandSlopeFactor = getParam<Scalar>("Problem.IslandSlopeFactor");        
        const Scalar islandCenterX = getParam<Scalar>("Problem.IslandCenterX");
        const Scalar islandCenterY = getParam<Scalar>("Problem.IslandCenterY");

        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            const auto eIdx = this->gridGeometry().elementMapper().index(element);
            const auto& globalPos = element.geometry().center();
            
            const auto radiusSquared = islandSlopeFactor * (((globalPos[0] - islandCenterX)* (globalPos[0] - islandCenterX))
                    + (globalPos[1] - islandCenterY)*(globalPos[1] - islandCenterY));
            
            using std::max;
            bedSurface_[eIdx] = max(0.0, islandHeightAtCenter - radiusSquared);
            
            if (globalPos[0] >= 0.9)
            {
                bedSurface_[eIdx] = 0.0;
            }


        }
    }

    /*! \brief Define the porosity in [-].
   *
   * \param globalPos The global position where we evaluate
   */
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return 1.0; }


    /*! \brief Define the gravitation.
    *
    * \return gravity constant
    */
    Scalar gravity(const GlobalPosition& globalPos) const
    {
        return gravity_;
    }

    /*! \brief Define the bed surface
    *
    * \param element The current element
    * \param scv The sub-control volume inside the element.
    * \return the bed surface
    */
    Scalar bedSurface(const Element& element,
                      const SubControlVolume& scv) const
    {
        return  bedSurface_[scv.elementIndex()];;
    }

private:
    static constexpr Scalar gravity_ = 9.81;
    std::vector<Scalar> bedSurface_;

};

} // end namespace Dumux

#endif
